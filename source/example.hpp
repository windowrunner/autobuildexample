//
//  example.hpp
//  helloworld
//
//  Created by Rudy Castan on 5/9/17.
//  Copyright © 2017 Rudy Castan. All rights reserved.
//
#pragma once

#include <string>

namespace project {
    std::string HelloWord()
    {
        return "안녕 세상아!\n";
    }
}
