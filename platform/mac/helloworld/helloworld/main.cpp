//
//  main.cpp
//  helloworld
//
//  Created by Rudy Castan on 5/9/17.
//  Copyright © 2017 Rudy Castan. All rights reserved.
//

#include <iostream>
#include "example.hpp"

int main(int argc, const char * argv[]) {
    std::cout << "Mac Platform\n";
    std::cout << project::HelloWord();
    return 0;
}
