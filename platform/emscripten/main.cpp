//
//  main.cpp
//  hellowrold
//
//  Created by Rudy Castan on 5/9/17.
//  Copyright © 2017 Rudy Castan. All rights reserved.
//

#include <iostream>
#include <emscripten.h>
#include "example.hpp"

extern "C" {

  //This macro Forces LLVM to not dead-code-eliminate a function
  //and exposes this function to javascript.
  EMSCRIPTEN_KEEPALIVE
  extern void print_browser(char* browser_name)
  {
  	if(browser_name != nullptr)
    	std::cout << browser_name << " Platform\n";
    else
    	std::cout << "Web Platform\n";
  }
}

#define MULTILINE(...) #__VA_ARGS__

int main(int argc, const char * argv[]) {
	//Run javascript to auto detect the browser type
	//Then call our print_browser function
    emscripten_run_script(MULTILINE(
    	function BrowserName() {
		    // Opera 8.0+
		    if ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0)
		        return 'Opera';

		    // Firefox 1.0+
		    if (typeof InstallTrigger !== 'undefined')
		        return 'Firefox';

		    // Safari 3.0+ "[object HTMLElementConstructor]" 
		    if (/constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification))
		        return 'Safari';

		    // Internet Explorer 6-11
		    if (/*@cc_on!@*/false || !!document.documentMode)
		        return 'Internet Explorer';

		    // Edge 20+
		    if (!!window.StyleMedia)
		        return 'Edge';

		    // Chrome 1+
		    if (!!window.chrome && !!window.chrome.webstore)
		        return 'Chrome';

		    return 'Unknown Browser';
		}
		Module.ccall('print_browser', 'null', ['string'], [BrowserName()]);
    	));
    std::cout << project::HelloWord();
    return 0;
}
