# AutoBuildExample #

This is a C++ project to use as an example for build automation.
Currently builds for Mac OS and the Web via [emscripten](https://github.com/kripken/emscripten).
The web version can be seen at [windowrunner.bitbucket.io](https://windowrunner.bitbucket.io)

## Setup ##

### Mac ###
To build for mac there is an xcode project file under platform/mac.
Alternatively you can use the mac_build.sh script under buildscripts.

### Web ###
To build for the web you can use the web_build.sh script under buildscripts. 
It assumes you have em++ defined in your PATH environment.

### Jenkins ###
To build this project with Jenkins: Checkout this repo.
Run the mac_build.sh script.
Run the web_build.sh script.
To update [windowrunner.bitbucket.io](https://windowrunner.bitbucket.io), run the publish.py script.

#### Links ####
* [Jenkins](https://jenkins.io)
* [Jenkins Official Intro](https://jenkins.io/doc/book/getting-started/)
* [Emscripten](http://kripken.github.io/emscripten-site/)
* [Emscripten Offical Intro](http://kripken.github.io/emscripten-site/docs/getting_started/index.html)
* [Match 3 Game](http://modern-cpp-examples.github.io/match3/)
* [Implementing A Web Game C++14 Youtube](https://www.youtube.com/watch?v=8gRHHIjx4oE)


### License ###
MIT License

Copyright (c) 2017 Rudy Castan

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.