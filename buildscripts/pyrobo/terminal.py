import re
import subprocess
import sys


def format_cmd(cmd):
    # Split string by spaces but keep spaces within quotes
    pattern = re.compile(r'''((?:[^\s"']|"[^"]*"|'[^']*')+)''')
    commands = pattern.split(cmd)[1::2]
    # remove outside double quotes. this makes commands like 'svn add "C:\temp\path name with space"' work
    commands = [w.strip().strip('"') for w in commands]
    return commands


def run(cmd, echo=True, exit_on_error=False, cwd=None, return_errorcode=False):
    formatted_cmd = format_cmd(cmd)
    if echo:
        print formatted_cmd
    proc = subprocess.Popen(formatted_cmd,
                            cwd=cwd,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    (out, err) = proc.communicate()
    if echo:
        if out:
            print out
        if err:
            print err
    if exit_on_error and proc.returncode != 0:
        sys.exit(proc.returncode)
    if not return_errorcode:
        return out, err
    else:
        return out, err, proc.returncode


def call(cmd, echo=True, cwd=None):
    formatted_cmd = format_cmd(cmd)
    if echo:
        print formatted_cmd
        return subprocess.call(formatted_cmd, cwd=cwd)

    proc = subprocess.Popen(formatted_cmd, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc.wait()
    return proc.returncode
