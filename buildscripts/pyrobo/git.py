import os
import re
import shutil
import terminal


class GitRepo(object):
    """This can be used to run git commands on a repo."""
    def __init__(self, repo_path, echo_git=False):
        self.repo_path = repo_path
        self.echo_git = echo_git

    def git(self, cmd, echo=None):
        if echo is None:
            echo = self.echo_git
        out, err = run(cmd, echo, self.repo_path)
        return out

    @property
    def branch_name(self):
        return self.git('rev-parse --abbrev-ref HEAD')

    def get_current_revision(self, relative_path=None):
        if not relative_path:
            return self.git('rev-parse --short HEAD').strip()
        return self.git('rev-parse --short HEAD:' + relative_path).strip()

    def get_last_changed_info(self, relative_path):
        author = self.git('log -1 --format=%cN ' + relative_path).strip()
        revision = self.git('log -1 --format=%h ' + relative_path).strip()
        date = self.git('log -1 --format=%cd ' + relative_path).strip()
        return author, revision, date

    def get_email_from(self, revision):
        # cE - commit email
        return self.git('log -1 --format=%cE ' + revision).strip()


def run(cmd, echo=True, cwd=None):
    out, err = terminal.run('git ' + cmd, exit_on_error=False, echo=echo, cwd=cwd)
    return out, err


def checkout(git_url, repo_dir=None):
    if not repo_dir:
        repo_dir = os.path.splitext(os.path.basename(git_url))[0]
    if os.path.isdir(repo_dir) and not os.path.isdir(os.path.join(repo_dir, '.git')):
        shutil.rmtree(repo_dir, ignore_errors=True)
    if not os.path.isdir(repo_dir):
        run('clone ' + git_url + ' ' + repo_dir)

    repo = GitRepo(repo_dir)
    repo.git('checkout -- .')
    repo.git('clean -fd')
    repo.git('status')
    repo.git('pull --tags --progress ' + git_url + ' +refs/heads/*:refs/remotes/origin/* --prune')
    repo.git('checkout master')
    return repo
