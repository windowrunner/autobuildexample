# Save current working directoy into wd
wd=$PWD

# Save root dir path of the project based off of this bach file
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}/" )/.." && pwd )"

#delete build dirs
[ -d "${wd}/bin/web" ] && rm -rf "${wd}/bin/web"

mkdir -p "${wd}/bin/web"

em++ -I"${DIR}/source" "${DIR}/platform/emscripten/main.cpp" -o "${wd}/bin/web/helloworld.html"
