# Save current working directoy into wd
wd=$PWD
# Save root dir path of the project based off of this bach file
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}/" )/.." && pwd )"

#delete build dirs
[ -d "${wd}/bin/mac/build" ] && rm -rf "${wd}/bin/mac/build"
[ -d "${wd}/bin/mac/build_tmp" ] && rm -rf "${wd}/bin/mac/build_tmp"

#build xcode project
xcodebuild -project "${DIR}/platform/mac/helloworld/helloworld.xcodeproj" CONFIGURATION_BUILD_DIR="${wd}/bin/mac/build" CONFIGURATION_TEMP_DIR="${wd}/bin/mac/build_tmp"
