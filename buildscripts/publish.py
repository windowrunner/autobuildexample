import base64
import os
import shutil
import pyrobo.git as git


def publish_webversion():
    git.checkout('git@bitbucket.org:windowrunner/windowrunner.bitbucket.io.git')
    publish_folder = 'windowrunner.bitbucket.io'
    for the_file in os.listdir(publish_folder):
        file_path = os.path.join(publish_folder, the_file)
        if os.path.isfile(file_path):
            os.remove(file_path)

    src_files = os.listdir('bin/web')
    for file_name in src_files:
        full_file_name = os.path.join('bin/web', file_name)
        if os.path.isfile(full_file_name):
            shutil.copy(full_file_name, publish_folder)

    os.rename(os.path.join(publish_folder, 'helloworld.html'), os.path.join(publish_folder, 'index.html'))

    repo = git.GitRepo(publish_folder)
    current_version = repo.get_current_revision()
    current_branch = repo.branch_name
    version_filename = os.path.join(publish_folder, base64.urlsafe_b64encode(current_version + current_branch))
    with open(version_filename + '.txt', 'w') as fh:
        fh.write(current_version + '\n')
        fh.write(current_branch + '\n')

    repo.git('add --all')
    repo.git('commit -m "Updating hello world html"')
    repo.git('push origin')

    print 'published to'
    print 'https://windowrunner.bitbucket.io'


if __name__ == '__main__':
    publish = os.getenv('PUBLISH_WEB_VERSION', 'true') == 'true'
    if publish:
        publish_webversion()
